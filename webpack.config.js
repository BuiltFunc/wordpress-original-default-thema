const path = require("path");
const TerserPlugin = require('terser-webpack-plugin');
const IS_PRODUCTION = process.env.NODE_ENV == "production" ? true : false;

module.exports = {
  // 'development' | 'production'
  devtool: 'source-map',

  watch: IS_PRODUCTION ? false : true,
  watchOptions: {
    ignored: /node_modules/
  },

  entry: {
    "main": path.resolve(__dirname, "./core/wordpress/wp-content/themes/original-theme/src/ts/Main.ts")
  },

  output: {
    path: path.resolve(__dirname, "./core/wordpress/wp-content/themes/original-theme/assets/js"),
    filename: "[name].bundle.js"
  },

  resolve: {
    extensions: [".ts", ".js"]
  },

  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader?cacheDirectory',
          },
          {
            loader: 'ts-loader',
          },
        ],
      }
    ]
  },

  optimization: {
    minimize: IS_PRODUCTION,
    minimizer: IS_PRODUCTION ?
      [new TerserPlugin({
        terserOptions: {
          compress: {
            drop_console: true,
          },
          output: {
            comments: false,
          },
        },
      })]
      : [],
  }
};