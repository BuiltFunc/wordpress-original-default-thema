<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

get_header();
?>

<main role="main">
	<div class="mainContentWrap">
		<article id="mainContent" class="mainContent">
			<?php
			if ( have_posts() ) {
				if ( is_page() ) {
					remove_filter( 'the_content', 'wpautop' );
				}
				while ( have_posts() ) {
					the_post();
					get_template_part( 'templates/elements/content' );
				}
			} else {
				get_template_part( 'templates/elements/content', 'none' );
			}
			?>
		</article>
	</div>
</main>

<?php
get_footer();
