export namespace TextSpanWrap {
  export const run = ($target: HTMLElement | Element) => {
    if ($target.hasChildNodes()) {
      const $nodes = [...$target.childNodes];

      let $spanWrapText = "";
      let $charCount = 0;

      $nodes.forEach(($node) => {

        if ($node.nodeType == 3) {//テキストの場合
          if ($node.textContent) {
            const text = $node.textContent.replace(/\r?\n|\s/g, '');//テキストから改行コード削除
            //spanで囲んで連結
            $spanWrapText = $spanWrapText + text.split('').reduce((acc, v) => {
              return acc + `<span class="js__textSpanWrap js__textSpanWrap--${$charCount++}">${v}</span>`;
            }, "");
          }
        } else {//テキスト以外
          //<br>などテキスト以外の要素をそのまま連結
          $spanWrapText = $spanWrapText + ($node as HTMLElement).outerHTML;
        }
      });

      $target.innerHTML = $spanWrapText;
    }
  }
}
