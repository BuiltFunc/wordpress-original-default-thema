export module MwWpFormModule {
  export class Service {
    constructor(language?: string) {
      document.addEventListener('readCompleteAction', function () {
        const $mw_wp_form = document.getElementsByClassName('mw_wp_form');
        if ($mw_wp_form && $mw_wp_form.length) {
          for (let $i = 0; $i < $mw_wp_form.length; $i++) {
            alert('MwWpFormModule');
            const $form = $mw_wp_form[$i];
            $form.setAttribute('action', '#' + $form.getAttribute('id'));
          }
        }
      }, false);
    }
  }
}
