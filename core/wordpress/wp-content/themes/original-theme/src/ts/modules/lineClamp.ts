export module LineClampModule {
  export class Service {
    constructor(language?: string) {
      document.addEventListener('readCompleteAction', () => {
        Service.lineClampCheck();
      });
    }

    /**
     * line-clamp非対応ブラウザか調べる
     */
    private static lineClampCheck() {
      const $elements = document.getElementsByClassName('js__lineClamp');
      if ($elements && $elements.length) {
        for (let $i = 0; $i < $elements.length; $i++) {
          const $element = $elements[$i] as HTMLElement;
          let $maxrow: string | null | number = $element.getAttribute('data-maxrow');
          if ($maxrow) {
            $maxrow = parseInt($maxrow);
          }
          if (typeof $maxrow === 'number' && 'WebkitLineClamp' in $element.style === false) {
            Service.lineClamp($element, $maxrow);

            window.addEventListener('resize', () => {
              if (typeof $maxrow === 'number') {
                Service.lineClamp($element, $maxrow);
              }
            });
          }
        }
      }
    }

    /**
     * line-clamp非対応ブラウザへの対応
     */
    private static lineClamp($element: HTMLElement, $maxrow: number) {
      let $element_height = $element.clientHeight;
      const $element_line_height = window.getComputedStyle($element).getPropertyValue('line-height');
      $element_height = Math.ceil(parseFloat($element_line_height) * $maxrow);

      // オリジナルの文章を取得
      let $org_text: string | null = $element.textContent;
      let $text: string | null = $org_text;

      // 対象の要素を、高さにautoを指定し非表示で複製する
      let $clone: HTMLElement;
      if ($element.previousElementSibling) {
        $clone = $element.previousElementSibling as HTMLElement;
        $org_text = $clone.textContent;
        $text = $org_text;
      } else {
        $clone = $element.cloneNode(true) as HTMLElement;
        $clone.style.display = 'none';
        $clone.style.maxHeight = 'none';
        $clone.style.height = 'auto';
        $clone.style.width = String($element.clientWidth);
        $clone.style.overflow = 'hidden';
      }
      if (!$text) {
        return;
      }

      // 目印クラスをつけて
      // DOMを追加
      $clone.classList.add('js__lineClamp--reset');
      if ($element.parentNode) {
        $element.parentNode.insertBefore($clone, $element);
      }

      // 指定した高さになるまで、1文字ずつ消去していく
      $clone.style.display = 'block';
      while (($text.length > 0) && ($clone.clientHeight > $element_height)) {
        $text = $text.substr(0, $text.length - 1);
        $clone.textContent = $text + '…';
      }
      $clone.style.display = 'none';
      $clone.textContent = $org_text;

      // 文章差し替え
      $element.textContent = $text + '…';
    }
  }
}