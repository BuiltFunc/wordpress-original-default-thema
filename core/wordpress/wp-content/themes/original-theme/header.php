<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

$user_agent = new UserAgent();
$device     = $user_agent->get_device();
$field_head = get_field( 'HEADタグ', 'option' );
$field_body = get_field( 'BODYタグ', 'option' );
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="format-detection" content="telephone=no">
	<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
	<?php echo do_shortcode( $field_head ); ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php echo do_shortcode( $field_body ); ?>
<?php
if ( ! is_page_template( 'templates/lp.php' ) ) {
	get_template_part( 'templates/elements/header' );
	if (
		! is_page_template( 'templates/single.php' ) && ! is_singular( 'post' ) &&
		! is_page_template( 'templates/archive.php' ) && ! is_archive() && ! is_home()
	) {
		Breadcrumb::the_breadcrumb();
	}
}
