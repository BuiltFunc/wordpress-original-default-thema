<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

get_template_part( 'templates/elements/footer' );
wp_footer();
?>
</body>
</html>
