<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * 管理画面でstyle追加
 */
class Admin_AddStyle {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
	}

	/**
	 * 管理画面でstyle追加
	 *
	 * @param string $hook_suffix suffix.
	 */
	public function admin_enqueue_scripts( $hook_suffix ) {
		wp_enqueue_style(
			'original-theme-admin-stylesheet',
			get_stylesheet_directory_uri() . '/functions/autoload-register/admin-addstyle/assets/css/style.css',
			array(),
			filemtime(
				get_stylesheet_directory() . '/functions/autoload-register/admin-addstyle/assets/css/style.css'
			)
		);
	}
}

new Admin_AddStyle();
