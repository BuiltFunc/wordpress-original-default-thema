<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * 管理画面でCodeMirrorを適用
 */
class Admin_CodeMirror {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'codemirror' ) );
	}

	/**
	 * 管理画面でCodeMirrorを適用
	 *
	 * @param string $hook_suffix suffix.
	 */
	public function codemirror( $hook_suffix ) {
		$settings['codeEditor'] = wp_enqueue_code_editor( array( 'type' => 'text/html' ) );
		if ( false === $settings ) {
			return;
		}
		wp_localize_script( 'jquery', 'codeEditorSettings', $settings );
		wp_enqueue_style( 'wp-codemirror' );
		wp_add_inline_script(
			'jquery',
			'jQuery(document).ready(function($) {
				let $_textarea = $(\'textarea\');
				if($_textarea.length){
					$_textarea.each(function(index, element) {
						if (!$(this).closest(\'.wp-editor-wrap\').length && !$(this).closest(\'.usces_admin\').length && !$(this).closest(\'#addtoany_admin_form\').length && !$(this).closest(\'#template\').length) {
							wp.codeEditor.initialize(element, codeEditorSettings );
						}
					})
				}
			})'
		);
		wp_add_inline_style(
			'wp-codemirror',
			'.CodeMirror {border: 1px solid #ddd;}'
		);
	}
}

new Admin_CodeMirror();
