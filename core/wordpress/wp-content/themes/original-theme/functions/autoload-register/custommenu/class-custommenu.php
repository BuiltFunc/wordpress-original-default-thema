<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * カスタムメニュー
 */
class CustomMenu {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'custom_menu' ) );
		add_filter( 'wp_nav_menu_items', array( $this, 'wp_nav_menu_items' ) );
	}

	/**
	 * ナビゲーションメニューを登録する
	 */
	public function custom_menu() {
		register_nav_menus(
			array(
				'global-nav'   => 'Global Menu',
				'footer-nav'   => 'Footer Menu (Left)',
				'footer-nav02' => 'Footer Menu (Right)',
			)
		);
	}

	/**
	 * 各アイテムを変換
	 *
	 * @param string $items .
	 */
	public function wp_nav_menu_items( $items ) {
		// wp_nav_menu hrer="#"　←の場合にaタグではなくspanに変換.
		$regex     = '/<a[^>]href\s?=\s?[\"\']#[\"\'][^>]*>(.*?)<\/a>/i';
		$match_num = preg_match_all( $regex, $items, $matches );
		if ( $match_num ) {
			foreach ( $matches[0] as $key => $value ) {
				$items = mb_ereg_replace( $value, "<a>{$matches[1][$key]}</a>", $items );
			}
		}

		// hrefの最初が「/」ならhome_url()を使う.
		$regex     = '@href="/(?!/)(.+)"@';
		$match_num = preg_match_all( $regex, $items, $matches );
		if ( $match_num ) {
			foreach ( $matches[0] as $key => $value ) {
				$replace = 'href="' . home_url( $matches[1][ $key ] ) . '"';
				$items   = mb_ereg_replace( $value, $replace, $items );
			}
		}

		return $items;
	}
}

new CustomMenu();

