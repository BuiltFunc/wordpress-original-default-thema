<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * リッチエディタ関連
 */
class Richedit {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_filter( 'user_can_richedit', array( $this, 'user_can_richedit' ) );
	}

	/**
	 * リッチエディタを無効化するフィルターフック
	 *
	 * @param bool $wp_rich_edit .
	 */
	public function user_can_richedit( $wp_rich_edit ) {
		$post_type = get_post_type();
		if ( 'page' === $post_type ) {
			return false;
		} else {
			return $wp_rich_edit;
		}
	}
}

new Richedit();
