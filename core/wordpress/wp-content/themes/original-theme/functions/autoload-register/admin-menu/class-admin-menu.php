<?php
/**
 * オリジナルテーマ
 *
 * Template Name: 準備中
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * 管理画面: メニューカスタマイズ
 */
class Admin_Menu {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'custom_menus' ), PHP_INT_MAX );
	}

	/**
	 * メニューカスタマイズ
	 */
	public function custom_menus() {
		global $menu,$submenu;
		foreach ( $menu as $key => $item ) {
			if ( 'MW WP Form' === $item[0] ) {
				$menu[ $key ][0] = 'お問い合わせ';
				if ( isset( $submenu[ $menu[ $key ][2] ] ) ) {
					foreach ( $submenu[ $menu[ $key ][2] ] as $sub_key => $sub_item ) {
						if ( 'MW WP Form' === $sub_item[0] ) {
							$submenu[ $menu[ $key ][2] ][ $sub_key ][0] = 'お問い合わせ一覧';
						}
					}
				}
			}
		}
	}
}

new Admin_Menu();
