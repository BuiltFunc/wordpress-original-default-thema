<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * パンクズリスト
 * Breadcrumb::the_breadcrumb()
 */
class Breadcrumb {
	/**
	 * パンクズリストを表示する
	 */
	public static function the_breadcrumb() {
		// トップページでは何も出力しないように.
		if ( is_front_page() ) {
			return false;
		}

		// そのページのWPオブジェクトを取得.
		$wp_obj = get_queried_object();

		?>
		<div id="breadcrumb" class="elementBreadcrumb">
			<ul>
				<li>
					<a href="<?php echo esc_url( home_url() ); ?>"><span>TOP</span></a>
				</li>
		<?php

		if ( is_attachment() ) {
			/**
			 * 添付ファイルページ ( $wp_obj : WP_Post )
			 * ※ 添付ファイルページでは is_single() も true になるので先に分岐
			 */
			$post_title = apply_filters( 'the_title', $wp_obj->post_title );
			?>
			<li><span><?php echo esc_html( $post_title ); ?></span></li>
			<?php

		} elseif ( is_single() ) {
			/**
			 * 投稿ページ ( $wp_obj : WP_Post )
			 */
			$post_id    = $wp_obj->ID;
			$post_type  = $wp_obj->post_type;
			$post_title = apply_filters( 'the_title', $wp_obj->post_title );

			// カスタム投稿タイプかどうか.
			if ( 'post' !== $post_type ) {
				$the_tax = '';
				// 投稿タイプに紐づいたタクソノミーを取得 (投稿フォーマットは除く).
				$tax_array = get_object_taxonomies( $post_type, 'names' );
				foreach ( $tax_array as $tax_name ) {
					if ( 'post_format' !== $tax_name ) {
						$the_tax = $tax_name;
						break;
					}
				}
				$post_type_link  = esc_url( get_post_type_archive_link( $post_type ) );
				$post_type_label = esc_html( get_post_type_object( $post_type )->label );

				// カスタム投稿タイプ名の表示.
				?>
				<li><a href="<?php echo do_shortcode( $post_type_link ); ?>"><span><?php echo esc_html( $post_type_label ); ?></span></a></li>
				<?php
			} else {
				$the_tax = 'category';
			}
			// 投稿に紐づくタームを全て取得.
			$terms = get_the_terms( $post_id, $the_tax );
			// タクソノミーが紐づいていれば表示.
			if ( false !== $terms ) {
				$child_terms  = array();
				$parents_list = array();
				// 全タームの親IDを取得.
				foreach ( $terms as $term ) {
					if ( 0 !== $term->parent ) {
							$parents_list[] = $term->parent;
					}
				}
				// 親リストに含まれないタームのみ取得.
				foreach ( $terms as $term ) {
					if ( ! in_array( $term->term_id, $parents_list, true ) ) {
						$child_terms[] = $term;
					}
				}
				// 最下層のターム配列から一つだけ取得.
				$term = $child_terms[0];
				if ( 0 !== $term->parent ) {
					// 親タームのIDリストを取得.
					$parent_array = array_reverse( get_ancestors( $term->term_id, $the_tax ) );
					foreach ( $parent_array as $parent_id ) {
						$parent_term = get_term( $parent_id, $the_tax );
						$parent_link = esc_url( get_term_link( $parent_id, $the_tax ) );
						$parent_name = esc_html( $parent_term->name );
						?>
						<li><a href="<?php echo do_shortcode( $parent_link ); ?>"><span><?php echo esc_html( $parent_name ); ?></span></a></li>
						<?php
					}
				}
				$term_link = esc_url( get_term_link( $term->term_id, $the_tax ) );
				$term_name = esc_html( $term->name );
				// 最下層のタームを表示.
				?>
				<li><a href="<?php echo do_shortcode( $term_link ); ?>"><span><?php echo esc_html( $term_name ); ?></span></a></li>
				<?php
			}

			// 投稿自身の表示.
			?>
			<li><span><?php echo esc_html( strip_tags( $post_title, null ) ); ?></span></li>
			<?php

		} elseif ( is_page() || is_home() ) {

			/**
			 * 固定ページ ( $wp_obj : WP_Post )
			 */
			$page_id    = $wp_obj->ID;
			$page_title = apply_filters( 'the_title', $wp_obj->post_title );

			// 親ページがあれば順番に表示.
			if ( 0 !== $wp_obj->post_parent ) {
				$parent_array = array_reverse( get_post_ancestors( $page_id ) );
				foreach ( $parent_array as $parent_id ) {
					$parent_link = esc_url( get_permalink( $parent_id ) );
					$parent_name = esc_html( get_the_title( $parent_id ) );
					?>
					<li><a href="<?php echo esc_attr( $parent_link ); ?>"><span><?php echo esc_html( $parent_name ); ?></span></a></li>
					<?php
				}
			}
			// 投稿自身の表示.
			?>
			<li><span><?php echo esc_html( strip_tags( $page_title, null ) ); ?></span></li>
			<?php

		} elseif ( is_post_type_archive() ) {

			/**
			 * 投稿タイプアーカイブページ ( $wp_obj : WP_Post_Type )
			 */
			?>
			<li><span><?php echo esc_html( $wp_obj->label ); ?></span></li>
			<?php

		} elseif ( is_date() ) {

			/**
			 * 日付アーカイブ ( $wp_obj : null )
			 */
			$year  = get_query_var( 'year' );
			$month = get_query_var( 'monthnum' );
			$day   = get_query_var( 'day' );

			if ( 0 !== $day ) {
				// 日別アーカイブ.
				?>
				<li><a href="<?php echo esc_url( get_year_link( $year ) ); ?>"><span><?php echo esc_html( $year ); ?>年</span></a></li>
				<li><a href="<?php echo esc_url( get_month_link( $year, $month ) ); ?>"><span><?php echo esc_html( $month ); ?>月</span></a></li>
				<li><span><?php echo esc_html( $day ); ?>日</span></li>
				<?php

			} elseif ( 0 !== $month ) {
				// 月別アーカイブ.
				?>
				<li><a href="<?php echo esc_url( get_year_link( $year ) ); ?>"><span><?php echo esc_html( $year ); ?>年</span></a></li>
				<li><span><?php echo esc_html( $month ); ?>月</span></li>
				<?php

			} else {
				// 年別アーカイブ.
				?>
				<li><span><?php echo esc_html( $year ); ?>年</span></li>
				<?php

			}
		} elseif ( is_author() ) {
			/**
			 * 投稿者アーカイブ ( $wp_obj : WP_User )
			 */
			?>
			<li><span><?php echo esc_html( $wp_obj->display_name ); ?> の執筆記事</span></li>
			<?php

		} elseif ( is_archive() ) {

			/**
			 * タームアーカイブ ( $wp_obj : WP_Term )
			 */
			$term_id   = $wp_obj->term_id;
			$term_name = $wp_obj->name;
			$tax_name  = $wp_obj->taxonomy;

			/* ここでタクソノミーに紐づくカスタム投稿タイプを出力しても良いでしょう。 */

			// 親ページがあれば順番に表示.
			if ( 0 !== $wp_obj->parent ) {

				$parent_array = array_reverse( get_ancestors( $term_id, $tax_name ) );
				foreach ( $parent_array as $parent_id ) {
					$parent_term = get_term( $parent_id, $tax_name );
					$parent_link = esc_url( get_term_link( $parent_id, $tax_name ) );
					$parent_name = esc_html( $parent_term->name );
					?>
					<li><a href="<?php echo esc_url( $parent_link ); ?>"><span><?php echo esc_html( $parent_name ); ?></span></a></li>
					<?php
				}
			}

			// ターム自身の表示.
			?>
			<li><span><?php echo esc_html( $term_name ); ?></span></li>
			<?php

		} elseif ( is_search() ) {

			/**
			 * 検索結果ページ
			 */
			?>
			<li><span>「<?php echo esc_html( get_search_query() ); ?>」で検索した結果</span></li>
			<?php

		} elseif ( is_404() ) {
			/**
			 * 404ページ
			 */
			?>
			<li><span>お探しの記事は見つかりませんでした。</span></li>
			<?php

		} else {
			/**
			 * その他のページ（無いと思うけど一応）
			 */
			?>
			<li><span><?php echo esc_html( get_the_title() ); ?></span></li>
			<?php

		}

		?>
		</ul></div>
		<?php
	}
}
