<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * ロゴ関連
 */
class Logo {
	/**
	 * ロゴ取得
	 */
	public static function get_logo() {
		$logo_alt   = get_bloginfo( 'name' );
		$logo       = $logo_alt;
		$logo_image = get_field( 'ロゴ', 'option' );
		if ( $logo_image ) {
			$logo = "<img src='{$logo_image}' alt='{$logo_alt}'>";
		}
		return $logo;
	}

	/**
	 * ロゴ取得
	 *
	 * @param string|null $logo_tag .
	 */
	public static function the_logo( $logo_tag = null ) {
		if ( empty( $logo_tag ) ) {
			$logo_tag = is_front_page() ? 'h1' : 'p';
		}
		$logo_href  = home_url();
		$logo_image = self::get_logo();
		echo do_shortcode( "<{$logo_tag} class='logo'><a href='{$logo_href}'>{$logo_image}</a></{$logo_tag}>" );
	}
}
