<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * MW WP Form 関連
 */
class Plugin_MwWpForm {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		// MW WP Form の自動改行処理を設定するフィルターフック.
		add_action( 'init', array( $this, 'mwform_content_wpautop_mw_wp_form' ) );
	}

	/**
	 * MW WP Form の自動改行処理を設定するフィルターフック
	 */
	public function mwform_content_wpautop_mw_wp_form() {
		$mw_wp_form_admin = new MW_WP_Form_Admin();
		$forms            = $mw_wp_form_admin->get_forms();
		foreach ( $forms as $form ) {
			add_filter( 'mwform_content_wpautop_mw-wp-form-' . $form->ID, '__return_false' );
		}
	}
}

if ( class_exists( 'MW_WP_Form_Admin' ) ) {
	new Plugin_MwWpForm();
}
