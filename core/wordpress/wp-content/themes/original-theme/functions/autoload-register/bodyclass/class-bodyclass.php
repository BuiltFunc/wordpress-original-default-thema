<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * テンプレートタグ/body class
 */
class BodyClass {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_filter( 'body_class', array( $this, 'body_class' ), 10 );
	}

	/**
	 * Fillter: body_class
	 *
	 * @param array $classes .
	 */
	public function body_class( $classes ) {
		$user_agent = new UserAgent();
		$device     = $user_agent->get_device();
		switch ( $device ) {
			case 'tablet':
				$classes[] = 'ua-tb';
				// not break.
			case 'mobile':
				$classes[] = 'ua-sp';
				break;
			default:
					$classes[] = 'ua-pc';
		}
		if ( is_front_page() ) {
			$classes[] = 'front-page';
		}
		if ( is_page() ) {
			$classes[] = 'page-slug-' . get_page_uri( get_the_ID() );
		}
		if ( get_option( 'front_screen', false ) ) {
			$classes[] = 'loading';
		}
		return $classes;
	}
}

new BodyClass();
