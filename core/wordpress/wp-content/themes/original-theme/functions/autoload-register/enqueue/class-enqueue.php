<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * CSS JavaScript読み込み
 */
class Enqueue {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ) );
	}

	/**
	 * CSS JavaScript読み込み
	 */
	public function enqueue() {
		// CSSの読み込み.
		if ( ! is_admin() ) {
			$filepath = get_stylesheet_directory() . '/assets/css/style.min.css';
			if ( file_exists( $filepath ) ) {
				wp_enqueue_style(
					'theme-stylesheet',
					get_stylesheet_directory_uri() . '/assets/css/style.min.css',
					array(),
					filemtime(
						get_stylesheet_directory() . '/assets/css/style.min.css'
					)
				);
			}
		}

		// JavaScriptの読み込み.
		if ( ! is_admin() ) {
			$filepath = get_stylesheet_directory() . '/assets/js/main.bundle.js';
			if ( file_exists( $filepath ) ) {
				wp_enqueue_script(
					'theme-main',
					get_stylesheet_directory_uri() . '/assets/js/main.bundle.js',
					array(),
					filemtime(
						$filepath
					),
					true
				);
			}
		}
	}
}

new Enqueue();
