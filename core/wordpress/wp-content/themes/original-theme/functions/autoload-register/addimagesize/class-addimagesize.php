<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * 初期設定: add_image_size
 */
class AddImageSize {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'add_theme_support' ) );
	}

	/**
	 * 初期設定: after_setup_theme
	 */
	public function add_theme_support() {
		// アイキャッチ.
		add_theme_support( 'post-thumbnails', array( 'post' ) );
		add_image_size( 'theme_large', 1920, 99999 );
		add_image_size( 'theme_middle', 960, 99999 );
	}
}

new AddImageSize();
