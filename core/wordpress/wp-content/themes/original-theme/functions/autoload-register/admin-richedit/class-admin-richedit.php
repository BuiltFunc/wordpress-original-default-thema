<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * ビジュアルエディターの無効化
 */
class Admin_RichEdit {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_action( 'user_can_richedit', array( $this, 'user_can_richedit' ) );
	}

	/**
	 * ビジュアルエディターの無効化
	 *
	 * @param boole $wp_rich_edit .
	 */
	public function user_can_richedit( $wp_rich_edit ) {
		global $post;
		if ( 'mw-wp-form' === $post->post_type ) {
			return false;
		} else {
			return $wp_rich_edit;
		}
	}
}

new Admin_RichEdit();
