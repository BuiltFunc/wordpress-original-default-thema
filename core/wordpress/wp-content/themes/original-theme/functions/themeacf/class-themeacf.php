<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * Acfに依存する設定
 */
class ThemeAcf {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		if ( function_exists( 'acf_add_local_field_group' ) ) {
			require dirname( __FILE__ ) . '/acf-generate.php';
		}
		add_action( 'acf/init', array( $this, 'acf_add_options_page' ) );

		// 投稿の個別ページのみパーマリンクを変更.
		add_filter( 'pre_post_link', array( $this, 'pre_post_link' ), PHP_INT_MAX );
		add_filter( 'post_rewrite_rules', array( $this, 'post_rewrite_rules' ), PHP_INT_MAX );

		// カスタムメニューにアンカーリンクを設定.
		add_filter( 'wp_nav_menu_objects', array( $this, 'wp_nav_menu_objects' ), PHP_INT_MAX, 2 );
	}

	/**
	 * カスタムメニューにアンカーリンクを設定
	 *
	 * @param object $menu_items .
	 * @param array  $args .
	 */
	public function wp_nav_menu_objects( $menu_items, $args ) {
		foreach ( $menu_items as &$menu_item ) {
			$anchorlink = get_field( 'アンカーリンク', $menu_item );
			if ( $anchorlink ) {
				$menu_item->url .= '#' . esc_attr( $anchorlink );
			}
		}

		return $menu_items;
	}

	/**
	 * 投稿の個別ページのみパーマリンクを変更
	 *
	 * @param string $permalink .
	 */
	public function pre_post_link( $permalink ) {
		$directory = get_field( '投稿ディレクトリ', 'option' );
		if ( $directory ) {
			$permalink = "/{$directory}" . $permalink;
		}
		return $permalink;
	}

	/**
	 * 投稿の個別ページのみパーマリンクを変更
	 *
	 * @param array $post_rewrite .
	 */
	public function post_rewrite_rules( $post_rewrite ) {
		$directory = get_field( '投稿ディレクトリ', 'option' );
		if ( $directory ) {
			$return_rule = array();
			foreach ( $post_rewrite as $regex => $rewrite ) {
				$return_rule[ trim( "{$directory}/{$regex}" ) ] = $rewrite;
			}
			$post_rewrite = $return_rule;
		}
		return $post_rewrite;
	}

	/**
	 * オプションページを作成
	 */
	public function acf_add_options_page() {
		if ( function_exists( 'acf_add_options_page' ) ) {
			acf_add_options_page(
				array(
					'page_title' => 'テーマオプション',
					'menu_title' => 'テーマオプション',
					'menu_slug'  => 'original-theme-options-general',
					'capability' => 'edit_posts',
					'redirect'   => false,
					'icon_url'   => 'dashicons-admin-settings',
					'position'   => 3,
				)
			);

			acf_add_options_sub_page(
				array(
					'page_title'  => 'オプション設定',
					'menu_title'  => 'オプション設定',
					'parent_slug' => 'theme-general-settings',
				)
			);
		}
	}
}

if ( function_exists( 'get_field' ) ) {
	$GLOBALS['ThemeAcf'] = new ThemeAcf();
}
