<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

// デバッグ.
if ( ! function_exists( 'debug' ) ) {
	/**
	 * デバッグ関数
	 *
	 * @param array|string|int $text .
	 * @param string           $label .
	 */
	function debug( $text, $label = '' ) {
		if ( is_array( $text ) || is_object( $text ) ) {
			echo '<pre>';
			if ( $label ) {
				echo esc_html( "{$label}: " );
			}
			print_r( $text );
			echo '</pre><br />';
			echo "\r\n";
		} elseif ( true === $text ) {
			echo '<pre>';
			if ( $label ) {
				echo esc_html( "{$label}: " );
			}
			echo 'TRUE';
			echo '</pre><br />';
			echo "\r\n";
		} elseif ( false === $text ) {
			echo '<pre>';
			if ( $label ) {
				echo esc_html( "{$label}: " );
			}
			echo 'FALSE';
			echo '</pre><br />';
			echo "\r\n";
		} elseif ( null === $text ) {
			echo '<pre>';
			if ( $label ) {
				echo esc_html( "{$label}: " );
			}
			echo 'NULL';
			echo '</pre><br />';
			echo "\r\n";
		} else {
			if ( $label ) {
				echo do_shortcode( "<pre>{$label}: {$text}</pre><br />" );
			} else {
				echo do_shortcode( "<pre>{$text}</pre><br />" );
			}
			echo "\r\n";
		}
	}
}

// autoload_register.
if ( ! function_exists( 'theme_autoload_register' ) ) {
	/**
	 * Autoload Register
	 *
	 * @param string $path .
	 */
	function theme_autoload_register( $path ) {
		$excludes = array(
			'.',
			'..',
		);
		$dirs     = scandir( $path );
		foreach ( $dirs as $dir ) {
			if ( in_array( $dir, $excludes, true ) ) {
				continue;
			}
			if ( true === is_dir( "{$path}/$dir" ) ) {
				$file_path = $path . "/{$dir}/class-{$dir}.php";
				if ( file_exists( $file_path ) ) {
					require $file_path;
				}
			}
		}
	}
}
