<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0.0
 */

/**
 * セキュリティ対策
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * カスタム投稿
 */
class CustomPost {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'register_post_types' ), PHP_INT_MAX );
	}

	/**
	 * カスタム投稿
	 */
	public function register_post_types() {
		/**
		 * TODO: コメントアウト
		$this->register_post_type(
			array(
				'post_type_label' => 'NEWS',
				'post_type'       => 'news',
				'post_type_slug'  => 'news',
				'menu_icon'       => '',
				'show_in_rest'    => true,
			)
		);
		 */
	}

	/**
	 * カスタム投稿作成
	 *
	 * @param array $_args .
	 */
	public function register_post_type( $_args ) {
		$post_type_label = $_args['post_type_label'];
		$post_type       = $_args['post_type'];
		$post_type_slug  = $_args['post_type_slug'];

		$labels = array(
			'name'                  => $post_type_label,
			'singular_name'         => $post_type_label,
			'add_new'               => '新規追加',
			'add_new_item'          => "新規{$post_type_label}を追加",
			'edit_item'             => "{$post_type_label}の編集",
			'new_item'              => "新規{$post_type_label}",
			'view_item'             => "{$post_type_label}を表示",
			'view_items'            => "{$post_type_label}の表示",
			'search_items'          => "{$post_type_label}を検索",
			'not_found'             => "{$post_type_label}が見つかりませんでした。",
			'not_found_in_trash'    => "ゴミ箱内に{$post_type_label}が見つかりませんでした。",
			'parent_item_colon'     => '',
			'all_items'             => "{$post_type_label}一覧",
			'archives'              => "{$post_type_label}アーカイブ",
			'attributes'            => "{$post_type_label}の属性",
			'insert_into_item'      => "{$post_type_label}に挿入",
			'uploaded_to_this_item' => "この{$post_type_label}へのアップロード",
			'featured_image'        => 'アイキャッチ画像',
			'set_featured_image'    => 'アイキャッチ画像を設定',
			'remove_featured_image' => 'アイキャッチ画像を削除',
			'use_featured_image'    => 'アイキャッチ画像として使用',
			'filter_items_list'     => "{$post_type_label}リストの絞り込み",
			'items_list_navigation' => "{$post_type_label}リストナビゲーション",
			'items_list'            => "{$post_type_label}リスト",
			'menu_name'             => $post_type_label,
			'name_admin_bar'        => $post_type_label,
		);

		$args = array(
			'labels'             => $labels,
			'public'             => ( isset( $_args['public'] ) ? $_args['public'] : true ),
			'publicly_queryable' => ( isset( $_args['publicly_queryable'] ) ? $_args['publicly_queryable'] : true ),
			'show_ui'            => ( isset( $_args['show_ui'] ) ? $_args['show_ui'] : true ),
			'show_in_menu'       => ( isset( $_args['show_in_menu'] ) ? $_args['show_in_menu'] : true ),
			'query_var'          => ( isset( $_args['query_var'] ) ? $_args['query_var'] : true ),
			'capability_type'    => ( isset( $_args['capability_type'] ) ? $_args['capability_type'] : 'post' ),
			'has_archive'        => ( isset( $_args['has_archive'] ) ? $_args['has_archive'] : true ),
			'hierarchical'       => ( isset( $_args['hierarchical'] ) ? $_args['hierarchical'] : false ),
			'menu_position'      => ( isset( $_args['menu_position'] ) ? $_args['menu_position'] : null ),
			'supports'           => ( isset( $_args['supports'] ) ? $_args['supports'] : array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ) ),
			'menu_icon'          => ( isset( $_args['menu_icon'] ) ? $_args['menu_icon'] : null ),
			'show_in_rest'       => ( isset( $_args['show_in_rest'] ) ? $_args['show_in_rest'] : false ),
			'rewrite'            => ( isset( $_args['rewrite'] ) ? $_args['rewrite'] : array(
				'slug'       => $post_type_slug,
				'with_front' => false,
			) ),
		);

		register_post_type( $post_type, $args );

		add_action(
			'rewrite_rules_array',
			function ( $rules ) use ( $post_type, $post_type_slug ) {
				$my_rule = array();
				$my_rule[ "{$post_type_slug}/([^/]+)(?:/([0-9]+))?/?$" ] = 'index.php?post_type=' . $post_type . '&name=$matches[1]&page=$matches[2]';
				$rules = array_merge( $rules, $my_rule );
				return $rules;
			}
		);
	}
}

new CustomPost();
