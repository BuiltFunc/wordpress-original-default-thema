const imagemin = require("imagemin-keep-folder");
const imageminMozjpeg = require("imagemin-mozjpeg");
const imageminPngquant = require("imagemin-pngquant");
const imageminGifsicle = require("imagemin-gifsicle");
const imageminSvgo = require("imagemin-svgo");
const { extendDefaultPlugins } = require("svgo");

imagemin([process.argv[2]], {
  plugins: [
    imageminMozjpeg({ quality: 100 }),
    imageminPngquant(),
    imageminGifsicle(),
    imageminSvgo({
      plugins: extendDefaultPlugins([
        { name: 'removeViewBox', active: false },
        { name: 'removeXMLProcInst', active: false }
      ])
    }),
  ],
  replaceOutputDir: output => {
    return output.replace(/images\//, "../assets/images/")
  },
}).then(() => {
  console.log("Images optimized");
  console.log(process.argv[2]);
})